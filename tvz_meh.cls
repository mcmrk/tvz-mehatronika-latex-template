% LaTeX predložak za pismene radove na
% Fakultetu elektrotehnike i ra�?unarstva.
% Verzija: 1.0
%
% Pokriveni radovi su: Seminar, Završni i Diplomski rad
%
% Opcije:
%  - seminar - podesi stil za Seminar (standardna postavka)
%  - diplomski - podesi stil za Diplomski rad
%  - zavrsni - podesi stil za Završni rad
%  - lmodern - koristi font lmodern (standardna postavka)
%  - times - koristi font times
%  - utf8 - u�?itaj inputenc paket i opcijom utf8 (standardna postavka)
%  - cp1250 - u�?itaj inputenc paket i opcijom cp1250
%  - authoryear - stil citiranja ``(author, year)'' (standardna postavka)
%  - numeric - stil citiranja ``[broj]''

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{tvz}

% Predefinirane vrijednosti opcija
\newif\if@fontlmodern		\global\@fontlmoderntrue
\newif\if@fonttimes			\global\@fonttimesfalse
\newif\if@radzavrsni		\global\@radzavrsnifalse
\newif\if@radseminar		\global\@radseminartrue
\newif\if@raddiplomski		\global\@raddiplomskifalse
\newif\if@citeauthoryear	\global\@citeauthoryeartrue
\newif\if@citenumeric		\global\@citenumericfalse
\newif\if@koristibibtex		\global\@koristibibtextrue
\newif\if@koristibiblatex	\global\@koristibiblatexfalse
\newif\if@encutf			\global\@encutftrue
\newif\if@enccp				\global\@enccpfalse

% Postavi font
\DeclareOption{lmodern}{\@fontlmoderntrue%
						\@fonttimesfalse}
\DeclareOption{times}{\@fonttimestrue%
					  \@fontlmodernfalse}
					  
% Tip rada
\DeclareOption{seminar}{\@radseminartrue%
					  \@radzavrsnifalse%
					  \@raddiplomskifalse}
\DeclareOption{zavrsni}{\@radseminarfalse%
					  \@radzavrsnitrue%
					  \@raddiplomskifalse}
\DeclareOption{diplomski}{\@radseminarfalse%
					  \@radzavrsnifalse%
					  \@raddiplomskitrue}
					  
% Encoding
\DeclareOption{utf8}{\@encutftrue%
					 \@enccpfalse}
\DeclareOption{cp1250}{\@encutffalse%
					 \@enccptrue}

% Na�?in citiranja
\DeclareOption{authoryear}{\@citeauthoryeartrue%
					       \@citenumericfalse}
\DeclareOption{numeric}{\@citeauthoryearfalse%
					    \@citenumerictrue}

\DeclareOption{koristibibtex}{\@koristibibtextrue%
						\@koristibiblatexfalse}
\DeclareOption{koristibiblatex}{\@koristibiblatextrue%
						\@koristibibtexfalse}

\ProcessOptions

\LoadClass[12pt, onecolumn]{report}

\RequirePackage[a4paper, left=3cm, right=2.5cm, bottom=3cm, top=3cm]{geometry}

% Postavljanje encodinga
\if@encutf \RequirePackage[utf8]{inputenc}
\else \if@enccp \RequirePackage[cp1250]{inputenc} \fi
\fi

\usepackage{ifpdf}

\RequirePackage[croatian]{babel}
\RequirePackage[T1]{fontenc}
\ifpdf
  \RequirePackage[pdftex]{graphicx}
\else
  \RequirePackage{graphicx} % Uklju�?eno jer je �?esto korišteno
\fi
\RequirePackage{amssymb} % Uklju�?eno jer je �?esto korišteno
\RequirePackage{amsmath} % Uklju�?eno jer je �?esto korišteno
\RequirePackage{fixltx2e}
\RequirePackage{caption}
\RequirePackage{ifthen}
\RequirePackage{url} % Potrebno radi natbiba
\RequirePackage{enumitem} % Potrebno radi izmjene itemize okoline

% Numeriranje literature kod seminara
\if@radseminar%
\RequirePackage[nottoc, notlof, notlot, numbib, chapter]{tocbibind}
\else
\RequirePackage[nottoc, notlof, notlot]{tocbibind}
\fi

% Postavljanje fonta
\if@fonttimes\RequirePackage{times} \fi
\if@fontlmodern\RequirePackage{lmodern} \fi

% Postavljanje stila citiranja
\if@koristibibtex
	\if@citeauthoryear \RequirePackage[authoryear, round]{natbib}
	\else \RequirePackage[numbers, square]{natbib}
	\fi
\else
	\RequirePackage[style=verbose-note,natbib=true,sortcites=true,defernumbers=true,pageref,block=space,backend=biber]{biblatex}
\fi

% Umjesto poziva \RequirePackage[outsidefoot]{pageno}.
% Neke LaTeX distribucije odbijaju automatski instalirati pageno paket.
% Stoga, dio paketa koji se koristi je kopiran u ovu cls datoteku.
\renewcommand{\ps@plain}{%
   \renewcommand{\@mkboth}{\@gobbletwo}%
   \renewcommand{\@oddhead}{}%
   \renewcommand{\@evenhead}{}%
%   \renewcommand{\@evenfoot}{\reset@font\rmfamily\thepage\hfil}% % <=== prebacuje na desnu stranu pagenum
%   \renewcommand{\@oddfoot}{\reset@font\rmfamily\hfil\thepage} % <=== prebacuje na desnu stranu pagenum
}
\pagestyle{plain}

\renewcommand{\captionlabelfont}{\bfseries}
\renewcommand{\captionfont}{\small}

% 1.5 prored, 1.3 je faktor za množenje
\linespread{1.3}

% Promjena naziva poglavlja ``Bibliografija'' -> ``Literatura''
\if@koristibibtex
	\addto\captionscroatian{%
		\def\bibname{Literatura}
	}
\else
	\DefineBibliographyStrings{croatian}{%
		bibliography = {Literatura},
	}
	\defbibheading{bibliography}[\bibname]{%
		% nenumerirani chapter (default):
		% \chapter*{#1}%
		% numerirani chapter:
		\chapter{#1}%
		\markboth{#1}{#1}
	}

	% U fusnot-citatu moramo imati ne samo ono što
	% style=verbose-note,pageref odradi, nego i ime
	% autora.
	% Prema odgovoru na TeX StackExchange:
	% http://tex.stackexchange.com/a/97580/25747
	\DeclareNameAlias{labelname}{first-last}
	\usepackage{xpatch}
	\xpatchbibmacro{footcite:note}{%
		\setunit*{\addcomma\space}%
		\printtext%
	}{%
		\setunit*{\addspace}%
		\printtext[parens]%
	}{}{}
	\xapptobibmacro{footcite:note}{\nopunct}{}{}% optional
	% Kraj http://tex.stackexchange.com/a/97580/25747

\fi

% Podešavanje oznaka kod listi (1. razina je crtica, 2. puni krug) 
\renewcommand{\labelitemi}{\textendash}
\renewcommand{\labelitemii}{\textbullet}

% http://www.tex.ac.uk/cgi-bin/texfaq2html?label=seccntfmt
% dodaje tocku nakon section broja ali ne i nakon chapter!
% za chapter se koristi jednostavno http://theoval.cmp.uea.ac.uk/~nlct/latex/thesis/node10.html
\renewcommand*{\@seccntformat}[1]{%
  \csname the#1\endcsname.\quad
}

% http://stackoverflow.com/questions/2426963/modifying-latex-table-of-contents-to-add-a-period-after-chapter-table-figure-numb
% Dodaje u toc tocke
\let \savenumberline \numberline
\def \numberline#1{\savenumberline{#1.}}

\renewcommand\theequation{\ifnum \c@chapter>\z@ \thechapter.\fi \@arabic\c@equation}
\renewcommand\thefigure{\ifnum \c@chapter>\z@ \thechapter.\fi \@arabic\c@figure}
\renewcommand\thetable{\ifnum \c@chapter>\z@ \thechapter.\fi \@arabic\c@table}

% Rimski brojevi stranica za zahvalu i tablicu sadržaja.
\renewcommand{\thepage}{\roman{page}}

\if@radseminar\else%
% Ispis napomene o umetanju izvornika.
\newcommand{\izvornik}{%
	\newpage
	\thispagestyle{empty}
	\vspace*{\fill}
	\hfil \textsl{Umjesto ove stranice umetnite izvornik Va\v{s}eg rada.} \par \hfil
	\hfil \textsl{Kako biste uklonili ovu stranicu, obri\v{s}ite naredbu \texttt{\textbackslash izvornik}.} \hfil
	\vspace*{\fill}
}
\fi

% Dodana zahvala
\newif\if@dodanazahvala		\global\@dodanazahvalafalse
\if@radseminar\else%
% Naredba za dodavanje zahvale.
\newcommand{\zahvala}[1]{%
	\newpage
	\thispagestyle{empty}
 	\setcounter{page}{2}
	\vspace*{\fill}
	\hfil {\itshape #1}\hfil
	\vspace*{\fill}
	\@dodanazahvalatrue
}
\fi

% TOC
\renewcommand\tableofcontents{%
   \if@dodanazahvala\else \setcounter{page}{5} \fi
   \if@radseminar \setcounter{page}{2} \fi
   \chapter*{\contentsname
   \@mkboth{%
   \MakeUppercase\contentsname}{\MakeUppercase\contentsname}}%
   \@starttoc{toc}%
   \thispagestyle{empty}
}

\newboolean{atappendix}
\setboolean{atappendix}{false}
\newboolean{secondpart}
\setboolean{secondpart}{false}

\renewcommand{\appendix}{
	\setboolean{atappendix}{true}
	\setcounter{chapter}{0}
	\renewcommand{\thechapter}{\Alph{chapter}}
}

\renewcommand{\@makechapterhead}[1]{%
\ifthenelse{\boolean{atappendix}}{%
	\vspace*{50\p@}%
	{\setlength{\parskip}{0em} \parindent \z@ \raggedright \normalfont
		\interlinepenalty\@M
		\ifnum \c@secnumdepth >\m@ne
			\Huge\bfseries Dodatak \thechapter \\ #1
			\par\nobreak
		\fi
		\nobreak
		\vskip 40\p@
	}
}
{
	\ifthenelse{\boolean{secondpart}}
	{
	}
	{
		\setcounter{page}{1}
		\renewcommand{\thepage}{\arabic{page}}
		\setboolean{secondpart}{true}
	}

	\vspace*{50\p@}%
	{\setlength{\parskip}{0em} \parindent \z@ \raggedright \normalfont
		\interlinepenalty\@M
		\ifnum \c@secnumdepth >\m@ne
			\Huge\bfseries \thechapter.~#1
			\par\nobreak
		\fi
		\nobreak
		\vskip 40\p@
	}
}
}

\renewcommand{\@makeschapterhead}[1]{%
\ifthenelse{\boolean{atappendix}}{%
	\vspace*{50\p@}%
	{\setlength{\parskip}{0em} \parindent \z@ \raggedright
		\normalfont
		\interlinepenalty\@Mdef
		\Huge\scshape Dodatak \\ #1\par
		\nobreak
		\vskip 40\p@
	}
}
{%
	\vspace*{50\p@}%
	{\setlength{\parskip}{0em} \parindent \z@ \raggedright
		\normalfont
		\interlinepenalty\@M
		\Huge\scshape #1\par
		\nobreak
		\vskip 40\p@
	}
}
}

\if@radseminar%
  \def\voditelj#1{\gdef\@voditelj{#1}}
  \def\@voditelj{\@latex@warning@no@line{Voditelj nije naveden.
  Koristi \noexpand\voditelj za definiranje}}
\else%
  \def\thesisnumber#1{\gdef\@thesisnumber{#1}}
  \def\@thesisnumber{\@latex@warning@no@line{Broj rada nije definiran.
  Koristi \noexpand\thesisnumber za definiranje}}

  \def\jmbag#1{\gdef\@jmbag{\vskip .5em%
      \large\sffamily\lineskip .75em%
      JMBAG: #1}}
  \def\@jmbag{\@latex@warning@no@line{JMBAG nije definiran.
  Koristi \noexpand\jmbag za definiranje}}
  
   \def\mentor#1{\gdef\@mentor{\vskip .5em%
      \large\sffamily\lineskip .75em%
      MENTOR: #1}}
  \def\@mentor{\@latex@warning@no@line{Mentor nije naveden.
  Koristi \noexpand\mentor za definiranje}}
\fi

\if@titlepage
	\renewcommand\maketitle{%
	\begin{titlepage}%
		\let\footnotesize\small
		\let\footnoterule\relax
	
		\begin{center}
			{\large\bfseries\sffamily\MakeUppercase{Tehni\v{c}ko veleu\v{c}ili\v{s}te u Zagrebu}}
			
			{\large\sffamily\MakeUppercase{Preddiplomski Stru\v{c}ni studij mehatronike}}
		\end{center}
		\vfill
		\begin{center}%

                \vskip 3em%
			
			{\large\sffamily\lineskip .75em \@author}

                        \if@radzavrsni%
                            \@jmbag
                        \fi
			\vskip 1em%

			{\huge \bfseries\sffamily \MakeUppercase \@title \par}%
			\vskip 1em%
			{

	    	\if@radzavrsni%
			  {\large\sffamily ZAVR\v{S}NI RAD br.~\@thesisnumber \par}%
		\else 
                    \if@raddiplomski%
			  {\large\sffamily DIPLOMSKI RAD br.~\@thesisnumber \par}%
%			\else {\large\sffamily\bfseries SEMINAR \par}%
	            \fi
                \fi
                \if@radzavrsni%
                            \@mentor
                        \fi
			\vskip 1em%

                        \large\sffamily\lineskip .75em%
			\begin{tabular}[t]{c}%
%	        	\if@radseminar%
%	        	  {\itshape \@author} \\
%	        	  Voditelj: {\itshape \@voditelj}
%	        	\else
%				    \@author
%				\fi
			\end{tabular}\par}%
		\end{center}\par
		\vfill
		\begin{center}
			{\sffamily\large Zagreb, 
			\ifcase\month\or
				sije\v{c}anj\or velja\v{c}a\or o\v{z}ujak\or travanj\or svibanj\or
	   	 		lipanj\or srpanj\or kolovoz\or rujan\or listopad\or studeni\or
	    			prosinac\fi \space \number\year.%
	    	}
		\end{center}
	\end{titlepage}%
	\setcounter{footnote}{0}%
	\global\let\maketitle\relax
% 	\global\let\@thanks\@empty
% 	\global\let\@author\@empty
	\global\let\@date\@empty
%	\global\let\@title\@empty
	\global\let\@jmbag\@empty
%	\global\let\title\relax
% 	\global\let\author\relax
	\global\let\date\relax
	\global\let\and\relax
	}
\fi

\newdimen\listindentation
\setlength{\listindentation}{3 em}

% Podešavanje izemize okoline
% Zahtjeva enumitem paket
\renewenvironment{itemize}%
{
	\setlength{\parskip}{0em}
	\advance\@itemdepth\@ne
	\edef\@itemitem{labelitem\romannumeral\@itemdepth}%

	\begin{list}{\csname\@itemitem\endcsname}{\setlength{\leftmargin}{\listindentation}\setlength{\itemsep}{0 em}\setlength{\topsep}{0 em}}
}%
{\end{list}}

% Naredba \engl
\newcommand{\engl}[1]{(engl.~\emph{#1})}

% Sažetak na hrvatskom
\newenvironment{sazetak}
{
\newpage
\setcounter{page}{3}
\vspace*{\fill}
\thispagestyle{empty}
\begin{center}
  {\bf \@title}
\end{center}
\hspace*{\fill} {\bf Sa\v{z}etak} \hspace*{\fill} \par
\vspace*{25pt}
}
{
\vspace*{\fill}
}

% Klju�?ne rije�?i na hrvatskom
\newcommand{\kljucnerijeci}[1]
{
\vspace{15pt}
\newline
\noindent \textbf{Klju\v{c}ne rije\v{c}i:} #1
}

% Klju�?ne rije�?i na engleskom
\newcommand{\keywords}[1]
{
\vspace{15pt}
\newline
\noindent \textbf{Keywords:} #1
}

% Sažetak na engleskom
\def\engtitle#1{\gdef\@engtitle{#1}}
\def\@engtitle{\@latex@warning@no@line{Engleski naziv rada nije definiran.
Koristi \noexpand\engtitle za definiranje}}
\renewenvironment{abstract}
{
\newpage
\vspace*{\fill}
\thispagestyle{empty}
\begin{center}
  {\bf \@engtitle}
\end{center}
\hspace*{\fill} {\bf Abstract} \hspace*{\fill} \par
\vspace*{25pt}
\setcounter{page}{4}
}
{
\vspace*{\fill}
}

% URLovi u \footcite nisu lijepi.
\if@koristibiblatex
\appto{\biburlsetup}{\renewcommand*{\UrlFont}{\normalfont\itshape}}
\fi

% Postavi svojstva PDFova
\RequirePackage[pdftex,unicode]{hyperref}
\AtBeginDocument {
  \hypersetup
  {
    pdfauthor={\@author},
    pdfsubject={},
    pdftitle={\@title},
    pdfkeywords={},
  }
}
\endinput
