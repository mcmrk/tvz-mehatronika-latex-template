# TVZ Mehatronika LaTeX Template
# README #

Ovo je LaTeX predložak za pisanje radova na Tehničkom veleučilištu u Zagrebu smjer Mehatronika. Specifično je izmijenjen za završne radove u nadi da će pomoći budućim generacijama studenata.

Predlošci su preuzeti i izmijenjeni prema FER LaTeX predlošcima:

https://code.google.com/archive/p/fer-latex-templates/downloads

Na gornjem linku se nalaze i vrlo dobre upute za korištenje LaTeX programskog jezika, treća datoteka po redu.



Isto tako vrijedi pogledati i sljedeći predložak za TVZ smjer računarstvo:

https://bitbucket.org/ivucica/tvz-latex-templates


## Kratke upute ##

Prije korištenja svakako pročitati upute sa prvog linka. Kako u tim uputama nije pokriveno apsolutno sve što je meni osobno bilo potrebno ovdje ću dati neke naputke. 

Prije nego krenete čitati dalje svakako pročitati stranice 4 i 5 uputa sa prvog linka.

Ovdje ću dati sljedeće naputke:
 
* Koje dodatne pakete je potrebno dodati
* Dodavanje naslova, autora rada, JMBAG-a i mentora
* Kako dodavati slike
* Kako dodavati programski kod povezivanjem datoteka programa sa diska
* Dodavanje popisa slika
* Dodavanje literature
* Dodavanje naslovnice završnog rada

Za sve ostale naredbe i probleme koje nisu pokrivene FERovim uputama i ovim naputcima najbolje je jednostavno guglati. Svi problemi na koje sam naišao korištenjem LaTeXa riješeni su na taj način.

Osnovna struktura **tex** datoteke kod korištenja ovog predloška mora biti:
```
\documentclass[zavrsni]{tvz_meh}
```
### Dodatni paketi ###

Da ne bi bilo problema sa korištenjem dijakritičkih znakova potrebno je aktivirati UTF - 8 encoding. Automatski kod aktiviranja encodinga stvara se ova linija koda:
```
% !TEX encoding = UTF-8 Unicode
```

Kod pisanja završnog rada bili su mi potrebni sljedeći paketi koji bi trebali pokriti manje više svaki završni rad:
```
\usepackage{graphicx}    %za dodavanje slika
\usepackage{listings}    %za dodavanje popisa
\setcitestyle{square}    %da literatura bude u uglatim zagradama []
\usepackage{subfig}      %nazivi slika
\usepackage{amsmath}     %za pisanje matematičkih formula
\usepackage{indentfirst} %da svaki paragraf bude uvučen
\usepackage{longtable}   %za dodavanje tablica
\usepackage{gensymb}     %za dodavanje specijanih znakova, stupnjevi, ohmi, ...
\usepackage[T1]{fontenc}
\usepackage[dvipsnames]{xcolor} %dodavanje boje tekstu
\usepackage{hyperref}    %dodavanje URL adresa
\usepackage{etoolbox}    %pisanje koda
\usepackage{natbib}      %dodavanje literature
\usepackage{wrapfig}     %postavljenje slika u razinu teksta
```
### Dodavanje naslova, autora rada, JMBAG-a i mentora ###
Kako bi dodali broj završnog rada, ime autora, JMBAG te ime mentora potrebno je upisati sljedeće naredbe:
```
\thesisnumber{broj rada upisati ovdje}
\title{ime rada}
\author{ime autora}
\jmbag{JMBAG broj}
\mentor{ime mentora}
```
### Dodavanje slika ###
Da bi dodali slike potrebno ih je staviti u isti folder u kojem se nalazi i ovaj predložak.
```
\begin{figure}[htb]
\centering
\includegraphics[scale=0.1]{imeDatoteke.jpg}  %skaliranje slike te ime datoteke sa tipom datoteke
\caption{Opis slike} %opis slike
\label{fig:nešto}    %proizvoljno ime slike koje će se koristiti u referenciranju
\end{figure}
```
Kako bi se kasnije u radu pozvali na sliku:
```
\ref{fig:nešto}
```
### Dodavanje programskog koda ###
Postoji nekoliko načina dodavanja programskog koda. Meni se najboljim činio način kojim se kod dodaje tako da se u folder sa predloškom doda datoteka programskog koda. Na taj način ako dođe do nekih izmjena u kodu potrebno je jednostavno zamijeniti datoteku koda novom datotekom istog imena.

Prvo je potrebno negdje u **tex** datoteci odrediti stil programskog koda. Ovdje je korišten sljedeći stil:
```
\lstdefinestyle{customc}{%
language=C++,                % choose the language of the code
basicstyle=\footnotesize,       % the size of the fonts that are used for the code
numbers=left,                   % where to put the line-numbers
numberstyle=\footnotesize,      % the size of the fonts that are used for the line-numbers
stepnumber=1,                   % the step between two line-numbers. If it is 1 each line will be numbered
numbersep=5pt,                  % how far the line-numbers are from the code
backgroundcolor=\color{white},  % choose the background color. You must add \usepackage{color}
showspaces=false,               % show spaces adding particular underscores
showstringspaces=false,         % underline spaces within strings
showtabs=false,                 % show tabs within strings adding particular underscores
frame=single,           % adds a frame around the code
tabsize=2,          % sets default tabsize to 2 spaces
captionpos=b,           % sets the caption-position to bottom
breaklines=true,        % sets automatic line breaking
breakatwhitespace=false,    % sets if automatic breaks should only happen at whitespace
escapeinside={\%*}{*)},          % if you want to add a comment within your code
keywordstyle=\bfseries\color{blue},
commentstyle=\itshape\color{ForestGreen},
identifierstyle=\color{black},
stringstyle=\color{orange}
}
```

Da bi dodali kod koristimo sljedeće naredbe:
```
\vspace{5mm}    %kako bi ispred koda bilo 5 mm praznog prostora
\lstinputlisting[language=C++, style=customc]{imeDatoteke.cpp}  %koji jezik koristimo, stil, i ime datoteke programskog koda
\vspace{5mm}    %kako bi ispod koda bilo 5 mm praznog prostora
```

### Dodavanje popisa slika ###
Za dodavanje popisa slika jednostavno se napišu sljedeće naredbe:
```
\cleardoublepage
\phantomsection
\addcontentsline{toc}{chapter}{\listfigurename}
\listoffigures
```

### Dodavanje literature ###
Literatura se može dodavati na nekoliko načina. Meni je najjednostavnije bilo na kraj **tex** datoteke dodati literaturu na sljedeći način.
```
\begin{thebibliography}{99}   %99 dozvoljava do 99 unosa
\bibitem{naziv}   %u viličastim zagradama staviti proizvoljni naziv literature kojim će se kasnije pozivati kod citiranja
  ime, naziv literature\\
  \url{web stranica}\\
  (datum pristupa: datum pristupa)

%ponoviti gornje tri linije za svaku stavku literature
%kada sve stavke dodate morate “zatvorit” literaturu sljedecom naredbom

\end{thebibliography}
```
Kod ovog načina pisanja literature može se dobivati sljedeći error kod compile-anja (Typeset-anja): *bibliography not compatible with author-year citations*.
Ja sam svaki puta stisnuo enter čime se nastavlja prevođenje te nije bilo nikakvih problema kod izgleda literature.

### Dodavanje naslovnice završnog rada ###
Za završni rad je potrebno napraviti dvije naslovnice, prvu sa imenom autora, nazivom rada te brojem završnog rada i druga na kojoj je potreban i JMBAG kao i ime mentora.

Kod korištenja LaTeXa to nije jednostavno te se taj problem može doskočiti tako da se napravi zaseban **tex** dokument sa prvom naslovnicom koji se zasebno ispisuje i dodaje kod uvezivanja završnog rada.

Da bi napravili zasebnu prvu stranicu (naslovnicu) bez JMBAG-a i imena mentora potrebno je upisati sljedeće naredbe u novi prazan **tex** dokument.
```
\documentclass[zavrsni]{tvz_meh}
\begin{document}
\thesisnumber{broj rada}
\title{naziv rada}
\author{ime autora}
\maketitle
\end{document}
```
Prevođenjem tog dokumeta dobiva se pdf datoteka prve stranice (naslovnice) koja se jednostavno dodaje kod uvezivanja završnog rada.